package main

import (
	"fmt"

	"gitlab.com/brunomurino/ssh-tunnel/app"
)

func main() {
	fmt.Println("Hello from App")
	app.Run()
}
