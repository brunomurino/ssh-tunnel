package open

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/mikkeloscar/sshconfig"
)

type OpenCmd struct {
	Name string `arg:"-n"`
}

func find_tunnels_configured() []sshconfig.SSHHost {

	path_to_ssh_config := filepath.Join(os.Getenv("HOME"), ".ssh", "config")

	hosts, err := sshconfig.Parse(path_to_ssh_config)
	if err != nil {
		fmt.Println(err)
	}

	var ssh_hosts []sshconfig.SSHHost

	for _, host := range hosts {
		if len(host.LocalForwards) > 0 {
			ssh_hosts = append(ssh_hosts, *host)
		}
	}

	return ssh_hosts
}

func open_tunnel(name string) {
	fmt.Printf("Openning tunnel %s\n", name)

	cmd := exec.Command("ssh", "-f", "-N", name)
	err := cmd.Start()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Printf("Tunnel started with PID %d\n", cmd.Process.Pid)
}

func get_user_choice_of_tunnel_to_open() string {
	configured_ssh_hosts := find_tunnels_configured()

	fmt.Println("Select which tunnel to open:")

	for index, ssh_host := range configured_ssh_hosts {
		fmt.Printf("  %d) %s\n", index+1, ssh_host.Host[0])
	}

	var host_number int
	fmt.Print("\nHost number: ")
	fmt.Scanf("%d", &host_number)

	ssh_tunnel_to_open := configured_ssh_hosts[host_number-1].Host[0]

	return ssh_tunnel_to_open
}

func OpenTunnel(args_open OpenCmd) {

	if args_open.Name != "" {
		open_tunnel(args_open.Name)
		return
	}

	ssh_tunnel_to_open := get_user_choice_of_tunnel_to_open()

	open_tunnel(ssh_tunnel_to_open)

}
