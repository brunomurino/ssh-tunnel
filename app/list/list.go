package list

import (
	"fmt"
	"log"
	"os/exec"
	"strconv"
	"strings"
)

type ListCmd struct{}

type OpenTunnel struct {
	PID  int
	Name string
}

func GetListOfOpenTunnels() []OpenTunnel {

	var list_of_open_tunnels []OpenTunnel

	cmd := exec.Command("bash", "-c", `ps -eo pid,command | grep "ssh -f" | grep -v "grep"`)
	stdout, err := cmd.Output()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exit_code := exitError.ExitCode()
			if exit_code == 1 {
				fmt.Println("No open tunnels found.")
				return list_of_open_tunnels
			}
			fmt.Printf("Command finished with error: %v\n", cmd.Process)
			log.Fatal(err)
		}
	}

	output := string(stdout)
	list_of_open_tunnels_raw := strings.Split(output, "\n")

	for _, open_tunnel := range list_of_open_tunnels_raw {
		if open_tunnel == "" {
			continue
		}

		open_tunnel_cleaned := strings.Trim(open_tunnel, " ")

		open_tunnel_ps_elements := strings.Split(open_tunnel_cleaned, " ")
		pid, err := strconv.Atoi(open_tunnel_ps_elements[0])
		if err != nil {
			log.Fatal(err)
		}
		tunnel_name := open_tunnel_ps_elements[4]

		list_of_open_tunnels = append(list_of_open_tunnels, OpenTunnel{PID: pid, Name: tunnel_name})
	}
	return list_of_open_tunnels
}

func ListOpenTunnels(args_list ListCmd) {
	list_of_open_tunnels := GetListOfOpenTunnels()

	if len(list_of_open_tunnels) == 0 {
		return
	}

	fmt.Printf("There are %d open tunnels: \n", len(list_of_open_tunnels))

	for i, open_tunnel := range list_of_open_tunnels {
		fmt.Printf("  %d) %+v\n", i+1, open_tunnel)
	}
}
