package kill

import (
	"fmt"
	"log"
	"os/exec"
	"strconv"

	"gitlab.com/brunomurino/ssh-tunnel/app/list"
)

type KillCmd struct {
	Remote      string `arg:"positional"`
	Branch      string `arg:"positional"`
	SetUpstream bool   `arg:"-u"`
}

func KillTunnel(args_kill KillCmd) {
	fmt.Println("Hello from Kill")

	list_of_open_tunnels := list.GetListOfOpenTunnels()

	for i, open_tunnel := range list_of_open_tunnels {
		fmt.Printf("  %d - Killing tunnel %s with PID %d\n", i, open_tunnel.Name, open_tunnel.PID)

		cmd := exec.Command("kill", strconv.Itoa(open_tunnel.PID))
		if err := cmd.Run(); err != nil {
			log.Fatal(err)
		}
	}

}
