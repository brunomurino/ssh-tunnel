package app

import (
	"github.com/alexflint/go-arg"
	"gitlab.com/brunomurino/ssh-tunnel/app/kill"
	"gitlab.com/brunomurino/ssh-tunnel/app/list"
	"gitlab.com/brunomurino/ssh-tunnel/app/open"
)

var args struct {
	List *list.ListCmd `arg:"subcommand:list"`
	Open *open.OpenCmd `arg:"subcommand:open"`
	Kill *kill.KillCmd `arg:"subcommand:kill"`
}

func Run() {

	arg.MustParse(&args)

	switch {
	case args.List != nil:
		list.ListOpenTunnels(*args.List)

	case args.Open != nil:
		open.OpenTunnel(*args.Open)

	case args.Kill != nil:
		kill.KillTunnel(*args.Kill)
	}
}
