module gitlab.com/brunomurino/ssh-tunnel

go 1.18

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/mikkeloscar/sshconfig v0.1.1
)

require (
	github.com/alexflint/go-scalar v1.1.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
)
